{ pkgs ? import <nixpkgs> {} }:
pkgs.mkShell {
   name = "cosmic-dev-shell";
   buildInputs = with pkgs; [
     # GPU drivers
     libGL mesa.drivers
     # Xorg packages
     xorg.libXi xorg.libXext xorg.libX11 
     xorg.libXv xorg.libXrandr zlib 
     # Misc
   ];
   shellHook = ''
      export EXTRA_CCFLAGS="-I/usr/include"
   '';          
}
