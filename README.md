# nix-shells

This holds nix-shell files for the nix package manager. For setting up nix on a non NixOS system such as Pop!_OS:

https://nix.dev/install-nix

## CUDA (nix-shells/CUDA/)

This is for setting up CUDA in a nix-shell environment.

## system76-docs (nix-shells/system76-docs/)

This is for setting up testing live changes to the system76/docs repo which uses Nodejs in a nix-shell environment.
